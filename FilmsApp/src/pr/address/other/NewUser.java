package pr.address.other;


/**
 * Class for collecting user infomation in one place.
 *
 */
public class NewUser {
	public String login;
	public String password;
	public String email;
	public String firstName;
	public String lastName;
	public String birthday;
	public String nickname;
	public String question;
	public String answer;
}
