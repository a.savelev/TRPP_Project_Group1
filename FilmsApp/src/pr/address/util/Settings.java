package pr.address.util;

/**
 * Class with setting for our database.
 *
 */
public final class Settings {
	//Put your settings here.
	
	public static final String dbUrl = "jdbc:postgresql://localhost:5432/FilmsApp";
	
	public static final String dbUser = "postgres";

	public static final String dbPassword = "1234";
	
	public static final String mailUser = "projectxyz0@gmail.com";
	
	public static final String mailPassword = "project123456";
	
	private Settings() { }
}
