package pr.address.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class for encrypting password.
 *
 */
public final class Crypt{
	
	public final static String encrypt(String target) {
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(target.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte b : sh.digest()) sb.append(Integer.toHexString(0xff & b));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
	
	private Crypt() { }
}
