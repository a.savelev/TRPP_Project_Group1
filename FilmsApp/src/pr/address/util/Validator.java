package pr.address.util;

import java.util.regex.Matcher;  
import java.util.regex.Pattern;

public class Validator {
	private final static Pattern emailPattern = Pattern.compile("[^@]+@[^@\\.]+\\.[^@]+");
	
	public static boolean validateEmail(String email){
		Matcher m = emailPattern.matcher(email);
			return m.matches();
		}
}
