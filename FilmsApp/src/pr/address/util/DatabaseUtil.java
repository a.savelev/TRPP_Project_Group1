package pr.address.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pr.address.other.NewUser;


/**
 * Class for working with database.
 *
 */
public final class DatabaseUtil {
	
	private static Connection connection;
	//private static Statement statement;
    //private static ResultSet result;
	
	private DatabaseUtil() throws SQLException { 
			connection = DriverManager.getConnection(Settings.dbUrl, Settings.dbUser, Settings.dbPassword);
	}
		
	private static DatabaseUtil singleton;
	
	/**
	 * Create a singleton DataBaseUtil class.
	 * @return DataBaseUtil
	 * @throws SQLException
	 */
	public static synchronized DatabaseUtil getDB() throws SQLException {
		if (singleton == null) {
			singleton = new DatabaseUtil();
		}
		return singleton;
	}
	
	public static Connection getConnection(){
		return connection;
	}
	
	/**
	 * Close statment and connection.
	 * 
	 */
	public boolean kill(){
		try {
			connection.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Function for formating strings.
	 * @param string
	 * @param args
	 * @return String
	 */
	private static String string(String string, Object... args) {
        return String.format(string, args);
    }
	
	/**
	 * Inserts new user in table 'users'.
	 * @param user
	 * @return true, if no exceptions have happened.
	 */
	public boolean insertUser(NewUser user){
		try {
			Statement statement = connection.createStatement();
			String querry = string("INSERT INTO users (login, password, email, first_name, last_name, birthday, nickname, question, answer) "
					+ "VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
					user.login, user.password, user.email, user.firstName, user.lastName, user.birthday, user.nickname, user.question, user.answer);
			
			statement.executeUpdate(querry);			
			statement.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Checking if login is exist or not.
	 * @param login
	 * @return true, if login is exist
	 */
	public boolean findLogin(String login) {
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT * FROM users WHERE login = '%s'", login);		
			ResultSet result = statement.executeQuery(querry);
			boolean isFound = false;
			if (result.next()) {
				isFound = true;
			}
			result.close();
			statement.close();
			return isFound;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Gives the password from database.
	 * @param login
	 * @return String password or null, if error has happened.
	 */
	public String getPassword(String login) {
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT password FROM users WHERE login = '%s'", login);		
			ResultSet result = statement.executeQuery(querry);
			String password = null;
			while(result.next()){
				password = result.getString("password");
			}
			result.close();
			statement.close();
			return password;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Returning an ArrayList with HashMap containing "id", "film", "rating"
	 * as a keys.
	 * 
	 * @param column
	 * @param columnValue 
	 * @return null, if error has happened.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList getFilmsPreview(String column, String columnValue){
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT id, name, year FROM films WHERE %s = '%s'", column, columnValue);
			ResultSet result = statement.executeQuery(querry);
			ArrayList<HashMap> filmList = listFormer(result);
			result.close();
			statement.close();
			return filmList;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList getFilmsYearPreview(int year){
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT id, name, year FROM films WHERE year BETWEEN %d AND %d", year, year+10);
			ResultSet result = statement.executeQuery(querry);
			ArrayList<HashMap> filmList = listFormer(result);
			result.close();
			statement.close();
			return filmList;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Collecting film's rating
	 * @param filmID
	 * @return rating, -1 if error has happened.
	 */
	public float getRating(int filmID){
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT AVG(rating) FROM rating WHERE film_id = %d", filmID);
			ResultSet result = statement.executeQuery(querry);
			int rate = 0;
			result.next();
			rate = result.getInt(1);
			result.close();
			statement.close();
			return rate;
			
		} catch (SQLException e) {
			// TODO ������������� ��������� ���� catch
			e.printStackTrace();
			return -1;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList listFormer(ResultSet result) throws SQLException{
		ArrayList<HashMap> filmList = new ArrayList<HashMap>();
		while(result.next()){
			HashMap temp = new HashMap();
			temp.put("id", result.getInt("id")); 
			temp.put("film", result.getString("name"));
			temp.put("year", result.getInt("year"));
			temp.put("rating", getRating(result.getInt("id")));
			filmList.add(temp);
		}
		return filmList;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap getUserInfo(String login){
		try {
			Statement statement = connection.createStatement();
			String querry = string("SELECT * FROM users WHERE login = '%s' ", login);
			ResultSet result = statement.executeQuery(querry);
			HashMap userInfo = new HashMap();;
			while(result.next()){	
				userInfo.put("id", result.getInt("id"));
				userInfo.put("login", result.getString("login"));
				userInfo.put("password", result.getString("password"));
				userInfo.put("email", result.getString("email"));
				userInfo.put("first_name", result.getString("first_name"));
				userInfo.put("last_name", result.getString("last_name"));
				userInfo.put("birthday", result.getString("birthday"));
				userInfo.put("nickname", result.getString("nickname"));
				userInfo.put("question", result.getString("question"));
				userInfo.put("answer", result.getString("answer"));	
				userInfo.put("image", result.getString("image"));	
			}
			result.close();
			statement.close();
			return userInfo;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteUser(String login){
		try {
			Statement statement = connection.createStatement();
			String querry = string("DELETE FROM users WHERE login = '%s'", login);
			statement.executeUpdate(querry);
			statement.close();
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static ObservableList<String> getList(String col, String table) { 
		Statement stmt; 
		try { 
				stmt = connection.createStatement(); 
				ObservableList<String> result = FXCollections.observableArrayList(); 
				String querry = string("SELECT %s FROM %s", col, table);
				ResultSet rs = stmt.executeQuery(querry); 
				while (rs.next()) { 
					result.add(rs.getString(col)); 
				} 
			rs.close();
			stmt.close(); 
			return result; 
		} catch (SQLException e) { 
			e.printStackTrace(); 
			return null; 
		} 
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList<HashMap> getMenuList(String table) { 
		Statement stmt; 
		try { 
				stmt = connection.createStatement(); 
				ArrayList<HashMap> result = new ArrayList<HashMap>();
				String querry = string("SELECT * FROM %s", table);
				ResultSet rs = stmt.executeQuery(querry); 
				while (rs.next()) { 
					HashMap temp = new HashMap();
					temp.put("id", rs.getInt("id"));
					temp.put(getCol(table), rs.getString(getCol(table)));
					temp.put("is_active", rs.getBoolean("is_active"));
					result.add(temp);
				} 
			rs.close();
			stmt.close(); 
			return result; 
		} catch (SQLException e) { 
			e.printStackTrace(); 
			return null; 
		} 
	}
	
	private static String getCol(String table){
		return table.substring(0, table.length()-1);
	}
	
	public static int getNumberOfRating(String id) {
		Statement stmt;
		try {
			String query = "SELECT count(*) FROM public.rating WHERE film_id = "+ id +";";
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
			int result = Integer.parseInt(rs.getString(1));
			stmt.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	
	public static Number getStatistic(String film_id,String rating) {
		Statement stmt;
		try {
			String query = "SELECT count(*) FROM public.rating WHERE film_id = "+ film_id +" and rating = " + rating + ";";
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
			int result = Integer.parseInt(rs.getString(1));
			stmt.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getId(String name, String table) {
		Statement stmt;
		try {
			String query = "SELECT id FROM " + table + " WHERE name = '"+ name + "';";
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
			String id = rs.getString(1);
			stmt.close();
			return id;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void updateUser(String first_name, String last_name, String birthday, String nickname, String login){
		Statement stmt;
		try {
			String query = string("UPDATE users SET first_name = '%s', last_name = '%s', birthday = '%s', nickname = '%s' WHERE login = '%s'", 
					first_name, last_name, birthday, nickname, login);
			stmt = connection.createStatement();
			stmt.executeUpdate(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void updateFilmRating(int filmId){
		Statement stmt;
		try {
			float rating = this.getRating(filmId);
			String query = "UPDATE films SET rating = "+ rating + " WHERE id = " + filmId;
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public HashMap getFilmInfo(int filmId){
		Statement stmt;
		try {
			float rating = this.getRating(filmId);
			String query = string("SELECT * FROM films WHERE id = %d",filmId);
			stmt = connection.createStatement();
			ResultSet res = stmt.executeQuery(query);
			HashMap map = new HashMap();
			res.next();
			map.put("name", res.getString("name"));
			map.put("genre", res.getString("genre"));
			map.put("director", res.getString("director"));
			map.put("year", res.getString("year"));
			map.put("price", res.getString("price"));
			map.put("country", res.getString("country"));
			res.close();
			stmt.close();
			return map;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void setRating(int rate, int filmId, String login){
		Statement stmt;
		try {
			int userId = getUserId(login);
			String query = string("INSERT INTO rating (user_id, film_id, rating) VALUES(%d, %d, %d)", userId, filmId, rate);
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			this.updateFilmRating(filmId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getUserId(String login){
		Statement stmt;
		try {
			String query = string("SELECT id FROM users WHERE login = '%s'", login);
			stmt = connection.createStatement();
			ResultSet res = stmt.executeQuery(query);
			res.next();
			int ID = res.getInt("id");
			res.close();
			stmt.close();
			return ID;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * Returns rating of the film set by use.
	 */
	public int getUserFilmRating(String login, int filmId){
		Statement stmt;
		try {
			int userId = getUserId(login);
			String query = string("SELECT rating FROM rating WHERE user_id = %d AND film_id = %d", userId, filmId);
			stmt = connection.createStatement();
			ResultSet res = stmt.executeQuery(query);
			if(!res.next()){
				return -2;
			}
			int rate = res.getInt("rating");
			res.close();
			stmt.close();
			return rate;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public void updateUserFilmRating(String login, int filmId, int rating){
		Statement stmt;
		try {
			int userId = this.getUserId(login);
			String query = "UPDATE rating SET rating = "+ rating + " WHERE film_id = " + filmId + " AND user_id = " + userId;
			stmt = connection.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			this.updateFilmRating(filmId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getRole(String login){
		Statement stmt;
		try {
			int userId = getUserId(login);
			String query = string("SELECT role FROM users WHERE user_id = %d",  userId);
			stmt = connection.createStatement();
			ResultSet res = stmt.executeQuery(query);
			res.next();
			String role = res.getString("rating");
			res.close();
			stmt.close();
			return role;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<HashMap> getMyRates(String login){
		Statement stmt;
		try {
			int userId = getUserId(login);
			String query = string("SELECT rating.rating, films.name FROM rating INNER JOIN films ON films.id = rating.film_id WHERE user_id = %d",  userId);
			stmt = connection.createStatement();
			ResultSet res = stmt.executeQuery(query);
			ArrayList<HashMap> list = new ArrayList<HashMap>();
			while(res.next()){
				HashMap map = new HashMap();
				map.put("film", res.getString("name"));
				map.put("rating", res.getInt("rating"));
				list.add(map);
			}
			res.close();
			stmt.close();
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws SQLException{
		DatabaseUtil db = new DatabaseUtil();
		ArrayList<HashMap> list = db.getMyRates("Login");
		System.out.println(list);
		
		db.kill();
	}
	
}



