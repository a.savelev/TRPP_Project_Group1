package pr.address;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pr.address.model.Decade;
import pr.address.model.Genre;
import pr.address.model.MenuComponent;
import pr.address.util.DatabaseUtil;
import pr.address.view.DeleteUserController;
import pr.address.view.EditUserDialogController;
import pr.address.view.FilmController;
import pr.address.view.LoginController;
import pr.address.view.MainController;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;

	private ObservableList<Genre> genres = FXCollections.observableArrayList();
	private ObservableList<Decade> decades = FXCollections.observableArrayList();
	private ObservableList<MenuComponent> userMenu = FXCollections.observableArrayList();

	private DatabaseUtil database;

	private String login;

	public MainApp() {
		try {
			database = DatabaseUtil.getDB();
		} catch (SQLException e) {
			// TODO ������������� ��������� ���� catch
			// ���� �� ������� ������������ � ��,
			// ���� �� ����� ��������� ����������...
			e.printStackTrace();
		}

		ArrayList<HashMap> gnrs = database.getMenuList("genres");
		for (HashMap gnr : gnrs) {
			genres.add(new Genre((String) gnr.get("genre")));
		}

		ArrayList<HashMap> dcds = database.getMenuList("decades");
		for (HashMap dcd : dcds) {
			decades.add(new Decade((String) dcd.get("decade")));
		}

		userMenu.add(new MenuComponent("��� �������"));
		userMenu.add(new MenuComponent("��� ������"));
		userMenu.add(new MenuComponent("��������� ������"));
	}

	public ObservableList<Genre> getGenres() {
		return genres;
	}

	public ObservableList<Decade> getDecades() {
		return decades;
	}

	public ObservableList<MenuComponent> getUserMenu() {
		return userMenu;
	}

	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("CinemaApp");

		initRootLayout();

		showMain();

		showLogin();
	}

	/**
	 * �������������� �������� �����.
	 */
	public void initRootLayout() {
		try {
			// ��������� �������� ����� �� fxml �����.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// ���������� �����, ���������� �������� �����.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ���������� � �������� ������ �������� �� ���������.
	 */
	public void showMain() {
		try {
			// ��������� �������� �� ���������.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Main.fxml"));
			AnchorPane main = (AnchorPane) loader.load();

			MainController controller = loader.getController();
			controller.setMainApp(this);

			// �������� �������� �� ��������� � ����� ��������� ������.
			rootLayout.setCenter(main);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showLogin() {
		try {
			// ��������� fxml-���� � ������ ����� �����
			// ��� ������������ ����������� ����.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Login.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// ������ ���������� ���� Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("����");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);

			dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				public void handle(WindowEvent we) {
					primaryStage.close();
				}
			});

			// ������� �������� � ����������.
			LoginController controller = loader.getController();
			controller.setLoginStage(dialogStage, this);
			controller.setDatabase(this.database);

			// ���������� ���������� ���� � ���, ���� ������������ ��� ��
			// �������
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showFilm(Integer id) {
		try {
			// ��������� fxml-���� � ������ ����� �����
			// ��� ������������ ����������� ����.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/film.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// ������ ���������� ���� Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("�����");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);

			// ������� �������� � ����������.
			FilmController controller = loader.getController();
			controller.setFilm(id, this);

			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * ���������� ������� �����.
	 *
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public DatabaseUtil getDatabase() {
		return database;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public boolean showEditUserDialog() {
		try {
			// ��������� fxml-���� � ������ ����� �����
			// ��� ������������ ����������� ����.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/EditUserDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// ������ ���������� ���� Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("�������������� ������������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// ������� �������� � ����������.
			EditUserDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage, this);
			controller.setText();

			// ���������� ���������� ���� � ���, ���� ������������ ��� ��
			// �������
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void showDeleteUserDialog() {
		try {
			// ��������� fxml-���� � ������ ����� �����
			// ��� ������������ ����������� ����.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/DeleteUserDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// ������ ���������� ���� Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("�������� ������������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// ������� �������� � ����������.
			DeleteUserController controller = loader.getController();
			controller.setDialogStage(dialogStage, this);

			dialogStage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
