package pr.address.view;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import pr.address.other.NewUser;
import pr.address.util.DatabaseUtil;


public class RegistrationConfirmationController {
	@FXML
	private TextField tokenField;
	
	private String token;
	
	private Stage ConfirmStage;
	
	private DatabaseUtil db;
	
	private NewUser newUser;
	
	@FXML
    private void initialize() {
    }
	
	@FXML
	private void handleConfirmation() {
		if(validConfirm()){
			db.insertUser(newUser);
			ConfirmStage.close();
		}
	}
	
	public boolean validConfirm(){
		if(token.equals(tokenField.getText())){
			
           return true;
		}  else{
				Alert alert = new Alert(AlertType.ERROR);
	           alert.initOwner(ConfirmStage);
	           alert.setTitle("������");
	           alert.setHeaderText("�������� �����!");
	           alert.setContentText("�������� �����!");
	
	           alert.showAndWait();
	           return false;
		}
		
	}
	
	public void setToken(String token){
		this.token = token;
	}
	
	public void setStage(Stage stage){
		ConfirmStage = stage;
	}
	
	public void setDB(DatabaseUtil db){
		this.db = db;
	}
	public void setNewUser(NewUser user){
		this.newUser = user;
	}
}

