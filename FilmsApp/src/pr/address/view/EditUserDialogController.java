package pr.address.view;

import java.util.HashMap;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import pr.address.MainApp;
import pr.address.util.DateUtil;

public class EditUserDialogController {
	@FXML
    private TextField nameField;
    @FXML
    private TextField surnameField;
    @FXML
    private TextField birthdayField;
    @FXML
    private TextField nicknameField;

    private Stage dialogStage;
    private boolean okClicked = false;
    private MainApp mainApp;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage, MainApp mainApp) {
        this.dialogStage = dialogStage;
        this.mainApp = mainApp;
        dialogStage.setResizable(false);
    }


    public void setText() {
    	HashMap user = mainApp.getDatabase().getUserInfo(mainApp.getLogin());
    	nameField.setText((String) user.get("first_name"));
    	surnameField.setText((String) user.get("last_name"));
    	birthdayField.setText((String) user.get("birthday"));
    	nicknameField.setText((String) user.get("nickname"));
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleOk() {
        if (isInputValid()) {
            mainApp.getDatabase().updateUser(nameField.getText(),
            		surnameField.getText(), birthdayField.getText(), nicknameField.getText(), mainApp.getLogin());
            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "������� ���!\n";
        }
        if (surnameField.getText() == null || surnameField.getText().length() == 0) {
            errorMessage += "������� �������!\n";
        }
        if (birthdayField.getText() == null || birthdayField.getText().length() == 0) {
            errorMessage += "������� ���� ��������!\n";
        } else {
            if (!DateUtil.validDate(birthdayField.getText())) {
                errorMessage += "�� ����� ������� ���� ��������. ����������� ������ ��.��.����!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // ���������� ��������� �� ������.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("�� ����� ������� ������");
            alert.setHeaderText("����������� ������� ������");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
