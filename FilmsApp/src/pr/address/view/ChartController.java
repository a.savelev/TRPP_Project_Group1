package pr.address.view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.*;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pr.address.util.DatabaseUtil;


public class ChartController implements Initializable{
	@FXML
	private PieChart  pie;
	@FXML
	private BarChart<String, Number>  bc;
	@FXML
    NumberAxis xAxis = new NumberAxis();
    @FXML
    CategoryAxis yAxis = new CategoryAxis();
    @FXML
	private ListView<String> genre;
    @FXML
	private ListView<String> films;
    String genreId;
    String filmId;
    @Override
	public void initialize(URL arg0, ResourceBundle arg1)  {
		films.setItems(DatabaseUtil.getList("name", "films"));
		genre.setItems(DatabaseUtil.getList("genre", "genres"));
	}
	@FXML
	public void getChart() {
		filmId = DatabaseUtil.getId(films.getSelectionModel().getSelectedItem() + "", "films");
		int num = DatabaseUtil.getNumberOfRating(filmId);
		drawPieChartForFilm();
		drawBarChartForFilm();
	}
	public void getChartForGenre() {
		genreId = DatabaseUtil.getId(genre.getSelectionModel().getSelectedItem() + "", "genres");
		int num = DatabaseUtil.getNumberOfRating(genreId);
		drawPieChartForGenre();
		drawBarChartForGenre();
	}
	private void drawPieChartForGenre(){
		
	}
     private void drawBarChartForGenre(){
		
	}
	private void drawPieChartForFilm(){
		int num = DatabaseUtil.getNumberOfRating(filmId);
		ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                // �������� ������ �� ��������� ����������� 
                new PieChart.Data("C ������� 1" + ", " + (int)DatabaseUtil.getStatistic(filmId,"1")*100/num + "%", (int)DatabaseUtil.getStatistic(filmId,"1")),
                new PieChart.Data("� ������� 2" + ", " + (int)DatabaseUtil.getStatistic(filmId,"2")*100/num + "%", (int)DatabaseUtil.getStatistic(filmId,"2")),
                new PieChart.Data("� ������� 3" + ", " + (int)DatabaseUtil.getStatistic(filmId,"3")*100/num + "%", (int)DatabaseUtil.getStatistic(filmId,"3")),
                new PieChart.Data("� ������� 4" + ", " + (int)DatabaseUtil.getStatistic(filmId,"4")*100/num + "%", (int)DatabaseUtil.getStatistic(filmId,"4")),
                new PieChart.Data("� ������� 5" + ", " + (int)DatabaseUtil.getStatistic(filmId,"5")*100/num + "%", (int)DatabaseUtil.getStatistic(filmId,"5")));
		
        pie.setData(pieChartData);
	}
	private void drawBarChartForFilm(){
        XYChart.Series<String, Number> series = new Series<String, Number>();
        series.setName("���������� �������������");
        series.getData().add(new Data<String, Number>("1", DatabaseUtil.getStatistic(filmId,"1")));
        series.getData().add(new Data<String, Number>("2", DatabaseUtil.getStatistic(filmId,"2")));
        series.getData().add(new Data<String, Number>("3", DatabaseUtil.getStatistic(filmId ,"3")));
        series.getData().add(new Data<String, Number>("4", DatabaseUtil.getStatistic(filmId ,"4")));
        series.getData().add(new Data<String, Number>("5", DatabaseUtil.getStatistic(filmId , "5")));
        bc.getData().clear();
        bc.getData().add(series);
	}

}