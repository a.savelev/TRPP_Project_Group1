package pr.address.view;

import static java.nio.file.StandardCopyOption.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Set;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pr.address.model.Content;
import pr.address.model.User;

public class FilmAdminController {
	
	public class Rating{
		public String userId;
		public int rate;
		public Rating(String id, int rate){
			userId = id;
			this.rate = rate;
		}
	}
	
	Content film;
	@FXML
	private TextField filmName, filmGenre, filmDirector, filmYear, filmPrice, filmCountry;
	@FXML
	private VBox filmBox;
	@FXML 
	private Label filmRating;
	@FXML
	private ImageView filmImage;
	private TableView<Rating> ratingTable = new TableView<Rating>();
	
	@FXML
	private void initialize(){
		setEditableMode(false);
		initTextFieldsListener();
	}
	
	public void setFilm(Content film){
		this.film = film;
		
		try{
			filmImage.setImage(new Image(new File(film.getImagePath()).toURI().toString()));
		}catch(Exception e){};
		
		filmRating.setText(Double.toString(film.getAvgRating()));
		filmName.setText(film.getName());
		filmGenre.setText(film.getGenre());
		filmDirector.setText(film.getDirector());
		filmYear.setText(Integer.toString(film.getYear()));
		try{
			filmPrice.setText(film.getPrice());
		}catch(NumberFormatException e){};
		filmCountry.setText(film.getCountry());
	}
	
	public void setEditableMode(boolean mode){
		filmName.setEditable(mode);
		filmGenre.setEditable(mode);
		filmDirector.setEditable(mode);
		filmYear.setEditable(mode);
		filmPrice.setEditable(mode);
		filmCountry.setEditable(mode);
		filmImage.setDisable(!mode);
		if(mode) addRatingTable();
	}
	
	private void initTextFieldsListener(){
		filmName.textProperty().addListener((observ, oldV, newV)->{
			film.setName(newV);
		});
		filmGenre.textProperty().addListener((observ, oldV, newV)->{
			film.setGenre(newV);
		});
		filmDirector.textProperty().addListener((observ, oldV, newV)->{
			film.setDirector(newV);
		});
		filmYear.textProperty().addListener((observ, oldV, newV)->{
			film.setYear(newV);
		});
		try{
			filmPrice.textProperty().addListener((observ, oldV, newV)->{
				film.setPrice(Integer.parseInt(newV));
			});
		}catch(NumberFormatException e){};
		filmCountry.textProperty().addListener((observ, oldV, newV)->{
			film.setCountry(newV);
		});
	}
	
	private void addRatingTable(){
		ObservableList<Rating> rating = FXCollections.observableArrayList();
		Map<String, Integer> map = film.getRating();
		System.out.println(map.keySet());
		for(String log: map.keySet())
			rating.add(new Rating(log, map.get(log)));
		
		TableColumn<Rating, String> userC = new TableColumn<Rating, String>();
		TableColumn<Rating, Integer> rateC = new TableColumn<Rating, Integer>();
		userC.setText("�����");
		rateC.setText("������");
		ratingTable.getColumns().add(userC);
		ratingTable.getColumns().add(rateC);
		
		ratingTable.setItems(rating);
		userC.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().userId));
		rateC.setCellValueFactory(field -> new SimpleIntegerProperty(field.getValue().rate).asObject());
		
		ratingTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		filmBox.getChildren().add(ratingTable);
	}
	
	@FXML
	private void imageListener(){
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("����� ������� ������");
		File img = fileChooser.showOpenDialog(new Stage());
		filmImage.setImage(new Image(img.toURI().toString()));
		File target = new File("src/img/"+img.getName());
		try {
			Files.copy(img.toPath(), target.toPath(), REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		film.setImagePath("src/img/"+img.getName());
		
	}
	
	
}
