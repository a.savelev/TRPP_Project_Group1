package pr.address.view;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pr.address.model.*;
import pr.address.util.DatabaseUtil;

public class adminController {

	//�������� ������� "������������"
	@FXML
	private TableView<User> usersTable;
	@FXML
	private TextField usersSearch;
	@FXML
	private Button userBlockBtn, userAnalBtn;
	@FXML
	private TableColumn<User, String> userId, userLogin, userEmail, userName, userSecondName, userNickname, userBirthday, userRole, userStatus;
	private ObservableList<User> users;
	
	//�������� ������� "�������"
	@FXML
	private TableView<Content> contentTable;
	@FXML
	private TextField contentSearch;
	@FXML
	private TableColumn<Content, String> contentId, contentStatus, contentName, contentGenre, contentYear, contentPrice, contentDirector, contentCountry;
	@FXML
	private Button contentChangeBtn, contentBlockSectionBtn, contentBlockFilmBtn, contentAddBtn, contentDeleteBtn;
	private ObservableList<Content> content;
	
	public adminController(){
		users = FXCollections.observableArrayList();
		content = FXCollections.observableArrayList();
	}
	
	@FXML
	private void initialize(){
		initUsers();
		initContent();
		fillTables();
		initUsersFilter();
		initContentFilter();
	}
	
	private void initContent(){
		Connection connect;
		try{
			DatabaseUtil.getDB();
			connect = DatabaseUtil.getConnection();
			Statement state = connect.createStatement();
			String query = "SELECT id FROM films";
			ResultSet result = state.executeQuery(query);
			while(result.next()){
				Content film = new Content();
				film.setId(result.getInt("id"));
				content.add(film);
			}
		}catch(Exception e){};
	}
	
	private void initUsers(){
		Connection connect;
		try{
			DatabaseUtil.getDB();
			connect = DatabaseUtil.getConnection();
			Statement state = connect.createStatement();
			String query = "SELECT id FROM users";
			ResultSet result = state.executeQuery(query);
			while(result.next()){
				User user = new User();
				user.setId(result.getInt("id"));
				users.add(user);
			}
		}catch(Exception e){};
	}
	
	private void fillTables(){
		//���������� ������� �������������
		usersTable.setItems(users);
		contentTable.setItems(content);
		userId.setCellValueFactory(field -> new SimpleStringProperty(Integer.toString(field.getValue().getId())));
		userLogin.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getLogin()));
		userEmail.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getEmail()));
		userName.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getFirstName()));
		userSecondName.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getLastName()));
		userNickname.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getNickname()));
		userBirthday.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getBirthday()));
		userRole.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getRole()));
		userStatus.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getStatus()));
		//���������� ������� ��������
		contentId.setCellValueFactory(field -> new SimpleStringProperty(Integer.toString(field.getValue().getId())));
		contentStatus.setCellValueFactory(field -> new SimpleStringProperty(Boolean.toString(field.getValue().getPublished())));
		contentName.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getName()));
		contentGenre.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getGenre()));
		contentYear.setCellValueFactory(field -> new SimpleStringProperty(Integer.toString(field.getValue().getYear())));
		contentPrice.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getPrice()));
		contentDirector.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getDirector()));
		contentCountry.setCellValueFactory(field -> new SimpleStringProperty(field.getValue().getCountry()));
	}
	
	private void initUsersFilter(){
		FilteredList<User> filteredData = new FilteredList<User>(users, p -> true);
       
        usersSearch.textProperty().addListener((observ, oldV, newV) -> {
            filteredData.setPredicate(obj -> {
            	return filter(newV, obj);
            });
        }); 
        SortedList<User> sortedData = new SortedList<User>(filteredData);
        sortedData.comparatorProperty().bind(usersTable.comparatorProperty());
        usersTable.setItems(sortedData);
    }
	
	private void initContentFilter(){
		FilteredList<Content> filteredData = new FilteredList<Content>(content, p -> true);
       
        contentSearch.textProperty().addListener((observ, oldV, newV) -> {
            filteredData.setPredicate(obj -> {
            	return filter(newV, obj);
            });
        }); 
        SortedList<Content> sortedData = new SortedList<Content>(filteredData);
        sortedData.comparatorProperty().bind(contentTable.comparatorProperty());
        contentTable.setItems(sortedData);
    }
	
	private boolean filter(String newV, Content content){
		if (newV == null || newV.isEmpty())
            return true;
		String lowerCaseFilter = newV.toLowerCase();
		if (Integer.toString(content.getId()).toLowerCase().indexOf(lowerCaseFilter) != -1)
	           return true;
		if (content.getName().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
		if (content.getGenre().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true;
		if (Integer.toString(content.getYear()).toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
		if (content.getPrice().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true;
		if (content.getDirector().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true;
		if (content.getCountry().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true;
        return false; 
    }
    
    private boolean filter(String newV, User user){
    	if (newV == null || newV.isEmpty())
            return true;
        String lowerCaseFilter = newV.toLowerCase();
        if (user.getLogin().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getEmail().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getFirstName().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getLastName().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getNickname().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getBirthday().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getRole().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        if (user.getStatus().toLowerCase().indexOf(lowerCaseFilter) != -1)
            return true; 
        return false; 
    }
	
	@FXML
	private void userAnalBtn(){
		User user = usersTable.getSelectionModel().getSelectedItem();
		if(user==null)
			return;
		if(user.getRole().equalsIgnoreCase("user"))
			user.setRole("analytic");
		else
			user.setRole("user");
		usersTable.refresh();
	}
	
	@FXML
	private void userBlockBtn(){
		User user = usersTable.getSelectionModel().getSelectedItem();
		if(user==null)
			return;
		if(user.getStatus().equalsIgnoreCase("active"))
			user.setStatus("blocked");
		else
			user.setStatus("active");
		usersTable.refresh();
	}
	
	@FXML
	private void contentChangeBtn(){
		try {
	        // ��������� fxml-���� � ������ ����� �����
	        // ��� ������������ ����������� ����.
	        FXMLLoader loader = new FXMLLoader(getClass().getResource("filmAdmin.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        FilmAdminController controller = loader.getController();
	        
	        // ������ ���������� ���� Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�����");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(usersTable.getScene().getWindow());
	        dialogStage.setScene(new Scene(page));

	        // ������� �������� � ����������.
	        try{
	        	controller.setFilm(contentTable.getSelectionModel().getSelectedItem());
	        }catch(NumberFormatException e){};
	        controller.setEditableMode(true);
	        // ���������� ���������� ���� � ���, ���� ������������ ��� �� �������
	        dialogStage.showAndWait();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	@FXML
	private void contentBlockSectionBtn(){
		Content film = contentTable.getSelectionModel().getSelectedItem();
		String genre = film.getGenre();
		for(Content orderFilm: content){
			if(orderFilm.getGenre().equalsIgnoreCase(genre))
				orderFilm.setPublished(false);
		}
		contentTable.refresh();
	}
	
	@FXML
	private void contentUnblockSectionBtn(){
		Content film = contentTable.getSelectionModel().getSelectedItem();
		String genre = film.getGenre();
		for(Content orderFilm: content){
			if(orderFilm.getGenre().equalsIgnoreCase(genre))
				orderFilm.setPublished(true);
		}
		contentTable.refresh();
	}
	@FXML
	private void contentAddBtn(){
		try {
			Content newFilm = Content.newFilm();
			
	        // ��������� fxml-���� � ������ ����� �����
	        // ��� ������������ ����������� ����.
	        FXMLLoader loader = new FXMLLoader(getClass().getResource("filmAdmin.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        FilmAdminController controller = loader.getController();
	        
	        // ������ ���������� ���� Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("�����");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(usersTable.getScene().getWindow());
	        dialogStage.setScene(new Scene(page));

	        // ������� �������� � ����������.
	        try{
	        	controller.setFilm(newFilm);
	        }catch(NumberFormatException e){};
	        controller.setEditableMode(true);
	        // ���������� ���������� ���� � ���, ���� ������������ ��� �� �������
	        dialogStage.showAndWait();
	        content.add(newFilm);
	        contentTable.refresh();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	@FXML
	private void contentBlockFilmBtn(){
		Content film = contentTable.getSelectionModel().getSelectedItem();
		if(film.getPublished() == true)
			film.setPublished(false);
		else
			film.setPublished(true);
		contentTable.refresh();
	}
	@FXML
	private void contentDeleteBtn(){
		Content film = contentTable.getSelectionModel().getSelectedItem();
		content.remove(film);
		film.delete();
		contentTable.refresh();
	}
}

