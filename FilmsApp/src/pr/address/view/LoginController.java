package pr.address.view;


import pr.address.MainApp;
import pr.address.other.NewUser;
import pr.address.util.Crypt;
import pr.address.util.DatabaseUtil;
import pr.address.util.DateUtil;
import pr.address.util.MailSender;
import pr.address.util.Validator;

import java.io.IOException;
import java.security.SecureRandom;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class LoginController {

	@FXML
	private TextField loginField;
	@FXML
	private PasswordField passField;
	@FXML
	private Label forgottedPassLabel;
	@FXML
	private TextField nameField;
	@FXML
	private TextField loginRegField;
	@FXML
	private PasswordField passRegField;
	@FXML
	private PasswordField checkPassRegField;
	@FXML
	private TextField surnameField;
	@FXML
	private TextField birthdayField;
	@FXML
	private TextField nicknameField;
	@FXML
	private ChoiceBox<String> secretQuestionField;
	@FXML
	private TextField secretAnswerField;
	@FXML
	private TextField emailField;

	private Stage loginStage;
	
	private MainApp mainApp;

	private DatabaseUtil db;
	
	private String token;


	@FXML
    private void initialize() {
		secretQuestionField.setItems(DatabaseUtil.getList("question", "secret_questions"));
    }

	public void setLoginStage(Stage loginStage, MainApp mainApp) {
			this.mainApp = mainApp;
	        this.loginStage = loginStage;
	}

	public void setDatabase(DatabaseUtil database){
		this.db = database;
	}

	@FXML
	private void handleLogin() {
		if (isLoginInputValid()) {
			mainApp.setLogin(loginField.getText());
			loginStage.close();
		}
	}

	private boolean isLoginInputValid() {
		String errorMessage = "";
		String login = loginField.getText();
		String password = passField.getText();
		boolean validLogin = true;

		if (login == null || login.length() == 0) {
            errorMessage += "������� �����!\n";
        } else if (!db.findLogin(login)){
        	 errorMessage += "������ ������ �� ����������!\n";
        	 validLogin = false;
        }

		if (password == null || password.length() == 0) {
            errorMessage += "������� ������!\n";
        } else if(validLogin){
			if(!db.getPassword(login).equals(Crypt.encrypt(password))){
				errorMessage += "�������� ������!\n";
			}
        }

		if (errorMessage.length() == 0) {
            return true;
        } else {
            // ���������� ��������� �� ������.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(loginStage);
            alert.setTitle("������");
            alert.setHeaderText("������� �� ��� ������");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }

	}

	@FXML
	private void handleRegistration() {
		if (isRegistrationInputValid()) {
			System.out.println("Ok");
			NewUser user = new NewUser();
			user.login = loginRegField.getText();
			user.password = Crypt.encrypt(passRegField.getText());
			user.email = emailField.getText();
			user.firstName = nameField.getText();
			user.lastName = surnameField.getText();
			user.birthday = birthdayField.getText();
			user.nickname = nicknameField.getText();
			user.question = secretQuestionField.getValue();
			user.answer = secretAnswerField.getText();
			
			sendEmail(user.email);
			showConfirmation(user);
		}
	}

	private boolean isRegistrationInputValid() {
		String errorMessage = "";
		boolean passCheck = true;

		if (loginRegField.getText() == null || loginRegField.getText().length() == 0) {
            errorMessage += "������� �����!\n";
        } else if (db.findLogin(loginRegField.getText())){
       	 	errorMessage += "����� ����� ��� ������������!\n";
        }


		if (passRegField.getText() == null || passRegField.getText().length() == 0) {
            errorMessage += "������� ������!\n";
        } else if (passRegField.getText().length() < 6){
        	errorMessage += "����������� ����� ������ 6 ��������!\n";
        }

		if (checkPassRegField.getText() == null || checkPassRegField.getText().length() == 0) {
            errorMessage += "��������� ������!\n";
        }

		if (nameField.getText() == null || nameField.getText().length() == 0) {
            errorMessage += "������� ���!\n";
        }

		if (surnameField.getText() == null || surnameField.getText().length() == 0) {
            errorMessage += "������� �������!\n";
        }

		if (birthdayField.getText() == null || birthdayField.getText().length() == 0) {
            errorMessage += "������� ���� ��������!\n";
		} else {
			if (!DateUtil.validDate(birthdayField.getText())) {
                errorMessage += "������������ ������ ���� - ��.��.����!\n";
			}
		}

		if (nicknameField.getText() == null || nicknameField.getText().length() == 0) {
            errorMessage += "������� ����� ���������!\n";
        }

		if (secretQuestionField.getValue() == null || secretQuestionField.getValue().length() == 0) {
            errorMessage += "������� ��������� ������!\n";
        }

		if (secretAnswerField.getText() == null || secretAnswerField.getText().length() == 0) {
            errorMessage += "������� ����� �� ��������� ������!\n";
        }

		if (emailField.getText() == null || emailField.getText().length() == 0) {
            errorMessage += "������� e-mail!\n";
        } else if (!Validator.validateEmail(emailField.getText())){
        	errorMessage += "E-mail ����������!\n";
        }

		if (errorMessage.length() == 0)
			if (!passRegField.getText().equals(checkPassRegField.getText())) {
				passCheck = false;
				errorMessage += ("�� ����� ������� ������ ������!\n");
			}

		if (errorMessage.length() == 0) {
            return true;
        } else {
            // ���������� ��������� �� ������.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(loginStage);
            alert.setTitle("������");
            if (passCheck)
            	alert.setHeaderText("������ �����");
            else
            	alert.setHeaderText("������ �� ���������");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
	}
	private void showConfirmation(NewUser user) {
        try {
            // ��������� fxml-���� � ������ ����� �����
            // ��� ������������ ����������� ����.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RegistrationConfirmation.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // ������ ���������� ���� Stage.
            Stage ConfirmStage = new Stage();
            ConfirmStage.setTitle("������������� �����������");
            ConfirmStage.initModality(Modality.WINDOW_MODAL);
            ConfirmStage.initOwner(loginStage);
            Scene scene = new Scene(page);
            ConfirmStage.setScene(scene);
            ConfirmStage.setResizable(false);

            ConfirmStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                   	loginStage.close();
                   	mainApp.getPrimaryStage().close();
                }
            });

            // ������� �������� � ����������.
            RegistrationConfirmationController controller = loader.getController();
            
            controller.setStage(ConfirmStage);
            controller.setToken(token);
            controller.setDB(this.db);
            controller.setNewUser(user);

            // ���������� ���������� ���� � ���, ���� ������������ ��� �� �������
            ConfirmStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	private void sendEmail(String email){
		SecureRandom srand = new SecureRandom();
		int tok = -1;
		while(tok <= 0){
			 tok = srand.nextInt();
		}
		token = Integer.toString(tok);
		String text = "��� ��� �������������: " + token;
		
		
		MailSender mailSender = new MailSender();
		mailSender.send("������������� �����������", text, email);
		System.out.println("Email sent");
	}
}
