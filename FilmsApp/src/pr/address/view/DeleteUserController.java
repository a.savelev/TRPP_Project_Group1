package pr.address.view;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import pr.address.MainApp;

public class DeleteUserController {

	private Stage dialogStage;
	private MainApp mainApp;
	private boolean yesClicked = false;

	@FXML
	private void initialize() {
	}

	public void setDialogStage(Stage dialogStage, MainApp mainApp) {
		this.dialogStage = dialogStage;
		this.mainApp = mainApp;
		dialogStage.setResizable(false);
	}

	public boolean isYesClicked() {
		return yesClicked;
	}

	@FXML
	private void handleYes() {
		mainApp.getDatabase().deleteUser(mainApp.getLogin());
		mainApp.getPrimaryStage().close();
	}

	@FXML
	private void handleNo() {
		dialogStage.close();
	}
}
