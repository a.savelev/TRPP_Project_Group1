package pr.address.view;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import pr.address.MainApp;
import pr.address.model.Decade;
import pr.address.model.Film;
import pr.address.model.Genre;
import pr.address.model.MenuComponent;
import pr.address.util.DatabaseUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

public class MainController {
	 @FXML
	 private TableView<Genre> genresTable;
	 @FXML
	 private TableColumn<Genre, String> genresColumn;
	 @FXML
	 private TableView<Decade> decadesTable;
	 @FXML
	 private TableColumn<Decade, String> decadesColumn;
	 @FXML
	 private TableView<MenuComponent> userMenuTable;
	 @FXML
	 private TableColumn<MenuComponent, String> userMenuColumn;
	 @FXML
	 private TableView<Film> filmsTable;
	 @FXML
	 private TableColumn<Film, String> filmNameColumn;
	 @FXML
	 private TableColumn<Film, String> filmYearColumn;
	 @FXML
	 private TableColumn<Film, String> filmRatingColumn;
	 @FXML
	 private TableColumn<Film, String> filmIdColumn;
     @FXML
	 private Label loginLabel;
	 @FXML
	 private Label nameLabel;
	 @FXML
	 private Label birthdayLabel;
	 @FXML
	 private Label surnameLabel;
	 @FXML
	 private Label emailLabel;
	 @FXML
	 private Label nicknameLabel;
	 @FXML
	 private AnchorPane adminAnchor;
	 @FXML
	 private AnchorPane chartAnchor;
	 @FXML
	 private TableView<Film> userRatesTable;
	 @FXML
	 private TableColumn<Film, String> ratedFilmNameColumn;
	 @FXML
	 private TableColumn<Film, String> rateColumn;

	  private ObservableList<Film> userFilms = FXCollections.observableArrayList();

	 private MainApp mainApp;
	 private DatabaseUtil db;

	 @FXML
	 private void initialize() {
	        // ������������� ������� ������
	        genresColumn.setCellValueFactory(cellData -> cellData.getValue().getGenreProperty());
	        // ������������� ������� �����������
	        decadesColumn.setCellValueFactory(cellData -> cellData.getValue().getDecadeProperty());
	        // ������������� ���� ������������
	        userMenuColumn.setCellValueFactory(cellData -> cellData.getValue().getMenuComponentProperty());
	 }

	 public void setMainApp(MainApp mainApp) {
	        this.mainApp = mainApp;
	        db = this.mainApp.getDatabase();

	        // ���������� � �������
	        genresTable.setItems(mainApp.getGenres());
	        decadesTable.setItems(mainApp.getDecades());
	        userMenuTable.setItems(mainApp.getUserMenu());

	        genresTable.getSelectionModel().selectedItemProperty().addListener(
	                (observable, oldValue, newValue) -> showFilmsByGenre(newValue));
	        decadesTable.getSelectionModel().selectedItemProperty().addListener(
	                (observable, oldValue, newValue) -> showFilmsByDecade(newValue));
	        filmsTable.getSelectionModel().selectedItemProperty().addListener(
	                (observable, oldValue, newValue) -> showFilmDialog(newValue));
	 }

	 private void showFilmsByGenre(Genre genre) {
		 	ObservableList<Film> films = FXCollections.observableArrayList();
		    if (genre != null) {

		        ArrayList<HashMap> filmsList = db.getFilmsPreview("genre", genre.getGenre());

		        for (HashMap filmItem : filmsList){
		        	 Film film = new Film ((String)filmItem.get("film"), (Integer)filmItem.get("year"), (Float)filmItem.get("rating"), (Integer)filmItem.get("id"));
		        	 films.add(film);
		        }
		    }

		    this.setFilmsTable(films);
	 }

	 private void showFilmsByDecade(Decade decade) {
		 	ObservableList<Film> films = FXCollections.observableArrayList();
		    if (decade != null) {
		    	ArrayList<HashMap> filmsList = db.getFilmsYearPreview(decade.getDecade());
		    	for (HashMap filmItem : filmsList){
		        	 Film film = new Film ((String)filmItem.get("film"), (Integer)filmItem.get("year"), (Float)filmItem.get("rating"), (Integer)filmItem.get("id"));
		        	 films.add(film);
		        }
		    }


		    this.setFilmsTable(films);
	 }

	 private void showFilmDialog(Film fm) {
		 mainApp.showFilm(fm.getId());
	 }

     private void showUserDetails() {
			HashMap user = db.getUserInfo(mainApp.getLogin());
			loginLabel.setText((String) user.get("login"));
			nameLabel.setText((String) user.get("first_name"));
			surnameLabel.setText((String) user.get("last_name"));
			birthdayLabel.setText((String) user.get("birthday"));
			emailLabel.setText((String) user.get("email"));
			nicknameLabel.setText((String) user.get("nickname"));
			
			ArrayList<HashMap> rates = db.getMyRates(mainApp.getLogin());
			for (HashMap map : rates){
				userFilms.add(new Film((String) map.get("film"), new Float((int) map.get("rating"))));
			}
			userRatesTable.setItems(userFilms);
			ratedFilmNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmNameProperty());
			rateColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmRatingProperty());
	 }

     @FXML
     private void handleUserProfileEnter() {
       	 showUserDetails();
     }

     @FXML
     private void handleChartEnter(){
    	 try {
	        	FXMLLoader loader = new FXMLLoader();
      	loader.setLocation(this.getClass().getResource("Chart.fxml"));
				AnchorPane chartLayout = (AnchorPane) loader.load();
				chartAnchor.getChildren().add(chartLayout);
				chartLayout.setPrefHeight(470);
				chartLayout.setPrefWidth(650);
			} catch (IOException e) {
				e.printStackTrace();
			}
     }

     @FXML
     private void handleAdminEnter(){
    	 try {
	        	FXMLLoader loader = new FXMLLoader();
         	loader.setLocation(this.getClass().getResource("admin.fxml"));
				AnchorPane adminLayout = (AnchorPane) loader.load();
				adminAnchor.getChildren().add(adminLayout);
				adminLayout.setPrefHeight(470);
				adminLayout.setPrefWidth(650);
			} catch (IOException e) {
				e.printStackTrace();
			}
     }

     @FXML
	 private void handleDeleteUser() {
    	 mainApp.showDeleteUserDialog();
	 }

     @FXML
     private void handleEditPerson() {
    	 boolean okClicked = mainApp.showEditUserDialog();
         if (okClicked) {
             showUserDetails();
         }
     }

	 private void setFilmsTable(ObservableList<Film> films) {
		 	filmsTable.setItems(films);
		    filmNameColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmNameProperty());
		    filmYearColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmYearProperty());
		    filmRatingColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmRatingProperty());
		    filmIdColumn.setCellValueFactory(cellData -> cellData.getValue().getFilmIdProperty());
	 }
}
