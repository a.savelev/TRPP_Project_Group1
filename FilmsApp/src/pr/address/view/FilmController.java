package pr.address.view;

import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pr.address.MainApp;
import pr.address.util.DatabaseUtil;

public class FilmController {
	@FXML
    private TextField filmName;
    @FXML
    private TextField filmGenre;
    @FXML
    private TextField filmDirector;
    @FXML
    private TextField filmYear;
    @FXML
    private TextField filmPrice;
    @FXML
    private TextField filmCountry;
    @FXML
	private ChoiceBox<Integer> yourRating;
    @FXML
    private ObservableList<Integer> ratings = FXCollections.observableArrayList();

    private Stage filmStage;

    private MainApp mainApp;
    
    Integer rate;

    int id;

    @FXML
    private void initialize() {
    	
    }

    public void setFilm(int id, MainApp mainApp) {
    	this.mainApp = mainApp;
    	this.id = id;

    	HashMap filmMap = mainApp.getDatabase().getFilmInfo(id);
    	
    	filmName.setText((String) filmMap.get("name"));
    	filmGenre.setText((String) filmMap.get("genre"));
    	filmDirector.setText((String) filmMap.get("director"));
    	filmYear.setText((String) filmMap.get("year"));
    	filmPrice.setText((String) filmMap.get("price"));
    	filmCountry.setText((String) filmMap.get("country"));    

    	ratings.add(new Integer(1));
    	ratings.add(new Integer(2));
    	ratings.add(new Integer(3));
    	ratings.add(new Integer(4));
    	ratings.add(new Integer(5));
		yourRating.setItems(ratings);
		
		rate = new Integer(mainApp.getDatabase().getUserFilmRating(mainApp.getLogin(), id));
		if(!(rate < 0)){
			yourRating.getSelectionModel().select(rate);
		}
    }

    @FXML
    private void handleRate() {
    	if(rate < 0){
    		mainApp.getDatabase().setRating((int) yourRating.getValue(), id, mainApp.getLogin());
    	} else {
    		mainApp.getDatabase().updateUserFilmRating(mainApp.getLogin(), id, (int) yourRating.getValue());
    	}
    }
}
