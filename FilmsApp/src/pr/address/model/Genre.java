package pr.address.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Genre {
	private final StringProperty genre;

	public Genre(String genre) {
		this.genre = new SimpleStringProperty(genre);
	}

	public StringProperty getGenreProperty() {
        return genre;
    }

	public String getGenre() {
		return genre.get();
	}
}
