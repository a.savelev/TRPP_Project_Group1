package pr.address.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MenuComponent {
	private final StringProperty comp;

	public MenuComponent(String comp) {
		this.comp = new SimpleStringProperty(comp);
	}

	public StringProperty getMenuComponentProperty() {
        return comp;
    }
}
