package pr.address.model;

import pr.address.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static pr.address.util.Crypt.encrypt;
import static pr.address.util.DateUtil.validDate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pr.address.other.NewUser;

public class Content {

	private int id;
	
	private Connection connection;
	
	public Content(){
		try {
			connection = DatabaseUtil.getDB().getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void delete(){
		try {
			Statement state = connection.createStatement();
			String query = "DELETE FROM films WHERE id="+id;
			state.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean setId(int id){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT MAX(id) FROM films";
			ResultSet result = state.executeQuery(query);
			result.next();
			int maxId = result.getInt(1);
			
			if(id<=maxId)
				this.id = id;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public int getId(){
		return id;
	}
	
	public Map<String, Integer> getRating(){
		Map<String, Integer> map = new HashMap<String, Integer>();
		try {
			Statement state = connection.createStatement();
			String query = "SELECT user_id, rating FROM rating WHERE film_id="+id;
			ResultSet result = state.executeQuery(query);
			while(result.next()){
				System.out.println(result.getString("user_id")+" "+result.getInt("rating"));
				map.put(result.getString("user_id"), result.getInt("rating"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return map;
	}
	
	public int getAvgRating(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT AVG(rating) FROM rating WHERE film_id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getName(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT name FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("name");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getGenre(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT genre FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("genre");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public int getYear(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT year FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getInt("year");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getPrice(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT price FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("price");
		} catch (Exception e) {}
		return null;
	}
	
	public String getDirector(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT director FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("director");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getCountry(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT country FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("country");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getImagePath(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT image FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("image");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean getPublished(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT published FROM films WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getBoolean("published");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setName(String name){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET name = '"+name+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setGenre(String genre){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET genre = '"+genre+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setYear(String year){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET year = '"+year+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setPrice(int price){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET price = '"+price+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {}
		return true;
	}
	
	public boolean setDirector(String director){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET director = '"+director+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setCountry(String country){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET country = '"+country+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setImagePath(String imagePath){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET image = '"+imagePath+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setPublished(boolean status){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE films SET published = '"+status+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public static Content newFilm(){
		Content film = new Content();
		try {
			DatabaseUtil.getDB();
			Statement state = DatabaseUtil.getConnection().createStatement();
			String query = "SELECT MAX(id) FROM films";
			ResultSet set = state.executeQuery(query);
			set.next();
			int max = set.getInt(1);
			max++;
			query = "INSERT INTO public.films(id, name, genre, year, price, director, country, image, published) VALUES ("+max+", '.', '.', 0, 0, '.', '.', '.', true)";
			state.executeUpdate(query);
			state = DatabaseUtil.getConnection().createStatement();
			film.setId(max);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return film;
	}
}
