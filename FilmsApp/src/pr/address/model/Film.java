package pr.address.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Film {
	private final StringProperty name;
	private final StringProperty year;
	private final StringProperty rating;
	private final StringProperty id;

	public Film(String name, Integer year, Float rating, Integer id) {
		this.name = new SimpleStringProperty(name);
		this.year = new SimpleStringProperty(year.toString());
		this.rating = new SimpleStringProperty(rating.toString());
		this.id = new SimpleStringProperty(id.toString());
	}
	
	public Film(String name, Float rating) {
		  this.name = new SimpleStringProperty(name);
		  this.rating = new SimpleStringProperty(rating.toString());
		  this.year = null;
		  this.id = null;
		 }

	public StringProperty getFilmIdProperty() {
		return id;
	}

	public StringProperty getFilmNameProperty() {
        return name;
    }

	public StringProperty getFilmYearProperty() {
        return year;
    }

	public StringProperty getFilmRatingProperty() {
        return rating;
    }

	public String getName() {
		return name.get();
	}

	public String getYear() {
		return year.get();
	}

	public String getRating() {
		return rating.get();
	}

	public Integer getId() {
		return Integer.parseInt(id.get());
	}
}
