package pr.address.model;

import pr.address.util.DatabaseUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import static pr.address.util.Crypt.encrypt;
import static pr.address.util.DateUtil.validDate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pr.address.other.NewUser;

public class User {
	private int id;
	private static Connection connection;
	
	public User(){
		try {
			connection = DatabaseUtil.getDB().getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean setId(int id){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT MAX(id) FROM users";
			ResultSet result = state.executeQuery(query);
			result.next();
			int maxId = result.getInt(1);
			
			if(id<=maxId)
				this.id = id;
			else
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public int getId(){
		return id;
	}
	
	public String getLogin(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT login FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("login");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getEmail(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT email FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("email");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getFirstName(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT first_name FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("first_name");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getLastName(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT last_name FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("last_name");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getNickname(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT nickname FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("nickname");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getBirthday(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT birthday FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("birthday");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRole(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT role FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("role");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getStatus(){
		try {
			Statement state = connection.createStatement();
			String query = "SELECT status FROM users WHERE id="+id;
			ResultSet result = state.executeQuery(query);
			result.next();
			return result.getString("status");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean setFirstName(String name){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET first_name = '"+name+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setPassword(String pass){
		try {
			Statement state = connection.createStatement();
			pass = encrypt(pass);
			String query = "UPDATE users SET password = '"+pass+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setEmail(String email){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET email = '"+email+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setLastName(String name){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET last_name = '"+name+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setNickame(String nickname){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET nickname = '"+nickname+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setQuestion(String question){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET question = '"+question+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setBirthday(String date){
		try {
			Statement state = connection.createStatement();
			if(!validDate(date))
				return false;
			String query = "UPDATE users SET birthday = '"+date+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setAnswer(String answer){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET answer = '"+answer+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setRole(String role){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET role = '"+role+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean setStatus(String status){
		try {
			Statement state = connection.createStatement();
			String query = "UPDATE users SET status = '"+status+"' WHERE id="+id;
			if(state.executeUpdate(query)==0)
				return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public static void main(String[] args){
		User newUser = new User();
		newUser.setId(1);
		
		System.out.println(newUser.getLogin());
		System.out.println(newUser.getEmail());
		System.out.println(newUser.getFirstName());
		System.out.println(newUser.getLastName());
		System.out.println(newUser.getNickname());
		System.out.println(newUser.getBirthday());
		System.out.println(newUser.getRole());
		System.out.println(newUser.getStatus());
	}
}