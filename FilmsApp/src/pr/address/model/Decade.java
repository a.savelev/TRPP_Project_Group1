package pr.address.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Decade {
	private final StringProperty decade;

	public Decade(String decade) {
		this.decade = new SimpleStringProperty(decade);
	}

	public StringProperty getDecadeProperty() {
        return decade;
    }

	public int getDecade() {
		String get = decade.get();
		String result = get.substring(0, 4);
		int year = Integer.parseInt(result);
		return year;
	}
}