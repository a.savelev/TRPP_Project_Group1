﻿CREATE TABLE secret_questions(
	id SERIAL NOT NULL PRIMARY KEY,
	question varchar(255) NOT NULL
);

CREATE TABLE users(
	id SERIAL NOT NULL PRIMARY KEY,
	login  varchar NOT NULL UNIQUE,
	password  varchar NOT NULL,
	email  varchar NOT NULL,
	first_name  varchar NOT NULL,
	last_name  varchar NOT NULL,
	nickname  varchar NOT NULL,
	birthday varchar(10) NOT NULL,
	question  varchar NOT NULL,
	answer  varchar NOT NULL,
	role varchar NOT NULL DEFAULT 'user',
	status varchar NOT NULL DEFAULT 'neactivirovan'
);

CREATE TABLE films(
	id SERIAL NOT NULL PRIMARY KEY,
	name  varchar NOT NULL,
	genre  varchar NOT NULL,
	year  int NOT NULL,
	price  money NOT NULL,
	director varchar NOT NULL,
	country varchar NOT NULL,
	image varchar,
	published boolean DEFAULT true
);

CREATE TABLE rating(
	id SERIAL NOT NULL PRIMARY KEY,
	user_id  int REFERENCES users(id) ON DELETE SET NULL,
	film_id  int NOT NULL REFERENCES films(id) ON DELETE CASCADE,
	rating int NOT NULL
);

CREATE TABLE genres(
	id SERIAL NOT NULL PRIMARY KEY,
	genre varchar NOT NULL,
	is_active boolean DEFAULT true	
);

CREATE TABLE decades(
	id SERIAL NOT NULL PRIMARY KEY,
	decade varchar NOT NULL,
	is_active boolean DEFAULT true	
);